<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\AdForm;
use \yii\web\ForbiddenHttpException;
use app\models\Ad;
use yii\data\Pagination;
use yii\web\HttpException; 

class SiteController extends Controller
{    
    /**
     * 
     * Declare access rules
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new  ForbiddenHttpException('You do not have access to this page.');
                },
                'only' => ['logout', 'edit', 'delete', 'create'],
                'rules' => [
                    [
                        'actions' => ['logout', 'edit', 'delete', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * 
     * Check access rights of the current user to the actions actionDelete, actionEdit
     */
    public function beforeAction($action) 
    {
        if (in_array($action->id, ['delete', 'edit'])) {
            
            if (!\Yii::$app->user->identity->isOwner(\Yii::$app->request->get('id'))) {
                \Yii::$app->session->setFlash('warning', 'You not allowed to change this ad');
                
                return $this->redirect('/')->send();
            }
        }
        
        return parent::beforeAction($action);
    }
    
    /**
     * 
     * Show list of ads
     */
    public function actionIndex()
    {
        $querry = Ad::find()->orderBy('id DESC');
        $pages = new Pagination([
            'totalCount' => $querry->count(), 
            'pageSize' => 5, 
            'forcePageParam' => false, 
            'pageSizeParam' => false,
        ]);
        $models = $querry->offset($pages->offset)->limit($pages->limit)->all();
       
        return $this->render('index', [
            'ads' => $models,
            'pages' => $pages,
        ]);
    }
    

    /**
     * Login action.
     *
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * 
     * Logout action
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * 
     * Show current ad
     */
    public function actionView($id) 
    {
        $ad = Ad::findOne($id);
        
        if (!$ad) {
            throw new HttpException(404, 'Ad with id = ' . $id . ' not found');
        }
        
        return $this->render('view', [
            'ad' => $ad,
        ]);
    }
    
    /**
     * 
     * Create new Ad
     */
    public function actionCreate()
    {
        $model = new AdForm();
        $button = 'Create';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'new Ad created');

            return $this->redirect('/');
        }
        
        return $this->render('edit', [
            'model' => $model,
            'button' => $button,
        ]);
    }
    
    /**
     * 
     * Edit current ad
     */
    public function actionEdit($id) 
    {
        $ad = Ad::findOne($id);
        
        if (!$ad) {
            throw new HttpException(404, 'Ad with id = ' . $id . ' not found');
        }
        
        $model = new AdForm();
        $model->title = $ad->title;
        $model->description = $ad->description;
        
        $button = 'Update';
        
        if ($model->load(Yii::$app->request->post()) && $model->update($id)) {
            Yii::$app->session->setFlash('success', 'Ad updated');

            return $this->redirect('/');
        }
        
        return $this->render('edit', [
            'model' => $model,
            'button' => $button,
        ]);
    }
    
    /**
     * 
     * Delete current ad
     */
    public function actionDelete($id)
    {
        $ad = Ad::getAd($id);
        
        if (!$ad) {
            throw new HttpException(404, 'Ad with id = ' . $id . ' not found');
        }
        
        if ($ad && $ad->deleteAd()) {
            \Yii::$app->session->setFlash('success', 'Ad deleted');
        }
        
        return $this->redirect('/');
    }

    /**
     * 
     * Show about page
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
