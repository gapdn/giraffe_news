<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ad}}`.
 */
class m190819_051536_create_ad_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ad}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'author_name' => $this->string()->notNull(),
            'is_active' => $this->integer()->defaultValue(1),
            'created_at' => $this->dateTime()->defaultValue(date('Y-m-d:h:m:s')),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ad}}');
    }
}
