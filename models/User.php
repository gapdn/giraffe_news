<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Ad;

/**
 * User model
 * 
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $auth_key
 */


class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() 
    {
        return $this->auth_key;
    }
    
    /**
     * 
     * @return string $username
     */
    public function getUsername() 
    {
        return $this->username;
    }
    
    public function getAds() 
    {
        return  $this->hasMany(Ad::class, ['author_name' => 'id']);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    public function validateAuthKey($authKey) 
    {
        return $this->getAuthKey() === $authKey;
    }
    
    public static function saveNewUser($username, $password)
    {
        $user = new User();
        $user->username = $username;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->save();
    
        return $user;
    }

    /**
     * @return Bool
     */
    public function isOwner($id) 
    {
        $ad = Ad::findOne($id);
        
        if ($ad && $this == $ad->user) {
            return true;
        }
        
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) 
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
}
