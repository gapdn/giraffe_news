<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class AdForm extends Model
{
    const MAX_DESCRIPTION_LENGHT = 1000;
    
    public $title;
    public $description;    

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],            
            [['description'], 'string', 'length' => [10, 1000]],
            [['title'], 'string', 'length' => [5, 100]],
        ];
    }

    public function save() 
    {
        if ($this->validate()) {
            $ad = new Ad();
            $ad->title = $this->title;
            $ad->description = $this->description;
            $ad->created_at = date('Y-m-d:h:m:s');
            $ad->author_name = Yii::$app->user->identity->getUsername();

            return $ad->save();
        }
        
        return false;
    }
    
    public function update($adId) 
    {
        if ($this->validate()) {
            $ad = Ad::findOne($adId);
            $ad->title = $this->title;
            $ad->description = $this->description;
            
            return $ad->save();
        }
        
        return false;
    }
}
