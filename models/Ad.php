<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\User;

/**
 * Ad model
 * 
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $author_name
 * @property string $created_at
 */


class Ad extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%ad}}';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
 
    public function getUser() 
    {
        return $this->hasOne(User::class, ['username' => 'author_name']);
    }
    
    public static function getAd($id) 
    {
        return self::findOne($id);
    }
    
    public static function getAllAds()
    {
        return self::find()->where(['is_active' => 1]);
    }
    
    public function deleteAd() 
    {
        return $this->delete();
    } 
    
    public function getShortDesc() 
    {
        return substr($this->description, 0, 42) . '...';
    }
}
