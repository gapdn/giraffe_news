<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'View';
?>
<div class="site-index">

    <div class="page-posts no-padding">                    
    <div class="row">                        
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">
                <div class="row">
                    <article class="post col-sm-12 col-xs-12">                                            
                        <div class="post-meta">
                            <div class="post-title">
                                <?php echo Html::encode($ad->title); ?>
                            </div>
                        </div>
                        <div class="post-description">
                            <p><?php echo HtmlPurifier::process($ad->description); ?></p>
                        </div>
                        <div class="post-bottom">
                            <div class="author-name">
                                <span><b>Author: </b><?php echo Html::encode($ad->user->username) ?></span>
                            </div>
                            <div class="post-date">
                                <span><?php echo Yii::$app->formatter->asDatetime($ad->created_at); ?></span>    
                            </div>
                            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->getId() == $ad->user->id): ?>
                            <div>
                                <a class="button" href="<?php echo Url::to(['delete', 'id' => $ad->id]) ?>">delete</a>
                                <span>|</span>
                                <a class="button" href="<?php echo Url::to(['edit', 'id' => $ad->id]) ?>">edit</a>
                            </div>                                    
                            <?php endif; ?>
                        </div>
                        <hr width="60%">
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
